
use("taxiDriver");
// Tạo bảng driver / như là user (là các tài xế)
db.createCollection("drivers");
// Tạo bảng history
db.createCollection("historydrivers");
// Tạo bảng tripDriver
db.createCollection("tripdrivers");
// Tạo bảng car
db.createCollection("cars");

db.createCollection("trips");

db.cars.insertMany([
  {
    _id: ObjectId("60aae4843ae33121e0de8501"),
    name: "Mercedes-Benz",
  },
  {
    _id: ObjectId("60aae4843ae33121e0de8502"),
    name: "BMW",
  },
  {
    _id: ObjectId("60aae4843ae33121e0de8503"),
    name: "Audi",
  },
  {
    _id: ObjectId("60aae4843ae33121e0de8504"),
    name: "Lexus",
  },
  {
    _id: ObjectId("60aae4843ae33121e0de8505"),
    name: "Toyota",
  },
]);

db.drivers.insertMany([
  {
    _id: ObjectId("60aae4843ae33121e0de8506"),
    dateofbirth: ISODate("1990-01-01"),
    gender: "male",
    name: "Nguyễn Nhất An",
    username: "0909878765",
    password: "$2b$10$vbUKrFNutR00mYVq3M.2kOS5VTC0rZBdtsyEWAHJSmcydxiAi5L4m",
    cars: [ObjectId("60aae4843ae33121e0de8501"), ObjectId("60aae4843ae33121e0de8502")],
  },
  {
    _id: ObjectId("60aae4843ae33121e0de8507"),
    dateofbirth: ISODate("1990-01-02"),
    gender: "female",
    name: "Nguyễn Văn Tèo",
    username: "0987654324",
    password: "$2b$10$vbUKrFNutR00mYVq3M.2kOS5VTC0rZBdtsyEWAHJSmcydxiAi5L4m",
    cars: [ObjectId("60aae4843ae33121e0de8501"), ObjectId("60aae4843ae33121e0de8503")],
  },
  {
    _id: ObjectId("60aae4843ae33121e0de4368"),
    dateofbirth: ISODate("1990-01-04"),
    gender: "male",
    name: "Nguyễn Trường Tuấn Kiệt",
    username: "0676542567",
    password: "$2b$10$vbUKrFNutR00mYVq3M.2kOS5VTC0rZBdtsyEWAHJSmcydxiAi5L4m",
    cars: [ObjectId("60aae4843ae33121e0de8503"), ObjectId("60aae4843ae33121e0de8505")],
  },
]);
// chuyến đang chờ
db.trips.insertMany([
  {
    _id: ObjectId("60aae4843ae33121e0de4111"),
    // orderNo: "123456",
    startLocation: "Hà Nội",
    endLocation: "Hải Phòng",
    distance: 100,
    price: 100000,
    status: "pending", 
    driverId: ObjectId("60aae4843ae33121e0de8506"),
  },
  {
    _id: ObjectId("60aae4843ae33121e0de4112"),
    // orderNo: "123457",
    startLocation: "Hà Nội",
    endLocation: "Hải Dương",
    distance: 50,
    price: 50000,
    status: "pending",
    driverId: ObjectId("60aae4843ae33121e0de8507"),
  },
  {
    _id: ObjectId("60aae4843ae33121e0de4113"),
    // orderNo: "123458",
    startLocation: "TP Hồ Chí Minh",
    endLocation: "Bình dương",
    distance: 75,
    price: 80000,
    status: "pending",
    driverId: ObjectId("60aae4843ae33121e0de4368"),
  }
]);
// những chuyến đã nhận
db.tripdrivers.insertMany([
  {
    _id: ObjectId("60aae4843ae33121e0de4222"),
    // orderNo: "111111",
    startLocation: "Bà Rịa Vũng Tàu",
    endLocation: "Hải Phòng",
    distance: 100,
    price: 100000,
    status: "accepted",
    driverId: ObjectId("60aae4843ae33121e0de8506"),
  },
  {
    _id: ObjectId("60aae4843ae33121e0de4333"),
    // orderNo: "111112",
    startLocation: "Bà Rịa Vũng Tàu",
    endLocation: "Bình dương",
    distance: 1000,
    price: 200000,
    status: "accepted",
    driverId: ObjectId("60aae4843ae33121e0de8506"),
  },
  {
    _id: ObjectId("60aae4843ae33121e0de4444"),
    // orderNo: "222222",
    startLocation: "Cần Thơ",
    endLocation: "Hải Dương",
    distance: 50,
    price: 50000,
    status: "accepted",
    driverId: ObjectId("60aae4843ae33121e0de8507"),
  },
  {
    _id: ObjectId("60aae4843ae33121e0de4555"),
    // orderNo: "333333",
    startLocation: "Đồng Nai",
    endLocation: "Bình dương",
    distance: 75,
    price: 80000,
    status: "accepted",
    driverId: ObjectId("60aae4843ae33121e0de4368"),
  }
]);
// xem lịch sử chuyến đã nhận của tài xế
db.historydrivers.insertMany([
  {
    historyId: 1,
    driverId: ObjectId("60aae4843ae33121e0de8506"),
    totalTrips: 2,
    totalEarnings: 300000,
    tripDrivers: [ObjectId("60aae4843ae33121e0de4222"), ObjectId("60aae4843ae33121e0de4333")]
  },
  {
    historyId: 2,
    driverId: ObjectId("60aae4843ae33121e0de8507"),
    totalTrips: 1,
    totalEarnings: 50000,
    tripDrivers: [ObjectId("60aae4843ae33121e0de4444")]
  },
  {
    historyId: 3,
    driverId: ObjectId("60aae4843ae33121e0de4368"),
    totalTrips: 1,
    totalEarnings: 80000,
    tripDrivers: [ObjectId("60aae4843ae33121e0de4555")]
  }
]);



