const Driver = require("../models/drivers");
const Car = require("../models/cars");

const bcrypt = require("bcrypt");


const getAllDriver = async (req, res) => {
    try {
        const drivers = await Driver.find().populate("cars");
        res.status(200).json(drivers);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
}

module.exports = {
    getAllDriver,
};
