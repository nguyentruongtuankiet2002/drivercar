
const Car = require("../models/cars");

const bcrypt = require("bcrypt");

const addCar = async (req, res) => {
    const { name } = req.body;
    try {
        const car = new Car({
            name: name,
        });
        await car.save();
        res.status(200).json(car);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};
const getAllCar = async (req, res) => {
    try {
        const cars = await Car.find();
        res.status(200).json(cars);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const getCarById = async (req, res) => {
    const { id } = req.params;
    try {
        const car = await Car.findById(id);
        if (!car) {
            return res.status(404).json({ error: "Car not found" });
        }
        res.status(200).json(car);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};


module.exports = {
    addCar,
    getAllCar,
    getCarById,
};
