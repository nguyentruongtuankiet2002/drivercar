const Driver = require("../models/drivers");
const Car = require("../models/cars");
const Tripdriver = require("../models/tripdrivers");
const axios = require('axios');
//retry
const axiosRetry = require('axios-retry').default;
//rate limiter client
const Bottleneck = require('bottleneck');
const axiosInstance = axios.create();

const limiter = new Bottleneck({
    minTime: 1000
});

axiosRetry(axiosInstance, {
    retries: 2,
    retryDelay: (retryCount) => retryCount * 3000
});


const getAllTripdriver = async (req, res) => {
    try {
        
        const tripdrivers = await Tripdriver.find().populate("driverId");
        for (let i = 0; i < tripdrivers.length; i++) {
            const driverId = tripdrivers[i].driverId._id; // Lấy ra id của driver
            const response = await limiter.schedule(() => axiosInstance.get(`${process.env.DRIVER_URL}/api/v1/driver/getDriverById/${driverId}`));
            const driverData = response.data;
            tripdrivers[i].driverId = driverData;
        }
        res.json(tripdrivers);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

const getTripdriverById = async (req, res) => {
    const { id } = req.params;
    try {
        const tripdriver = await Tripdriver.findById(id);
        const driverId = tripdriver.driverId._id; // Lấy ra id của driver
        const response = await axios.get(`${process.env.DRIVER_URL}/api/v1/driver/getDriverById/${driverId}`);
        const driverData = response.data;
        tripdriver.driverId = driverData;
        const newTripdriver = ({
            _id: tripdriver._id,
            startLocation: tripdriver.startLocation,
            endLocation: tripdriver.endLocation,
            distance: tripdriver.distance,
            price: tripdriver.price,
            status: tripdriver.status,
            driverId: driverData,
        });
        res.json(newTripdriver);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}


const acceptTrip = async (req, res) => {
    const { id } = req.params;
    try {
        const tripResponse = await axios.get(`${process.env.TRIP_URL}/api/v1/trips/getTripById/${id}`);

        if (tripResponse.status === 200) {
            const trip = tripResponse.data;
            const newTrip = new Tripdriver({
                _id: trip._id,
                startLocation: trip.startLocation,
                endLocation: trip.endLocation,
                distance: trip.distance,
                price: trip.price,
                status: 'accepted',
                driverId: trip.driverId._id
            });

            await newTrip.save();
            try {
                await axios.delete(`${process.env.TRIP_URL}/api/v1/trips/deleteTrip/${id}`);
                res.json(newTrip);
            } catch (error) {
                console.error(`Failed to delete trip with id ${id}: ${error.message}`);
                res.status(500).json({ message: `Failed to delete trip with id ${id}` });
            }
        } else {
            res.status(500).json({ message: 'Unable to accept trip' });
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

module.exports = {
    getAllTripdriver,
    getTripdriverById,
    acceptTrip
};
