const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const driverSchema = new Schema({
    dateofbirth: {
        type: Date,
        required: true,
        default: Date.now()
    },
    gender: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    cars: [{
        type: Schema.Types.ObjectId,
        ref: "Car"
    }]
});

const Driver = mongoose.model("Driver", driverSchema);
module.exports = Driver;
