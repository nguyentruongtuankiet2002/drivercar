const { Router } = require("express");
const tripdriverController = require("../controller/tripdriversController");

const router = Router();

// http://localhost:3000/api/v1/users/login
router.get("/getAllTripdriver", tripdriverController.getAllTripdriver);
// http://localhost:3000/api/v1/users/sinup
router.get("/getTripdriverById/:id", tripdriverController.getTripdriverById);

router.put("/acceptTrip/:id", tripdriverController.acceptTrip);


module.exports = router;