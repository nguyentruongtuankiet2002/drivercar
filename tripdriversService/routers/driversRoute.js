const { Router } = require("express");
const driversController = require("../controller/driversController");

const router = Router();

// lấy ra danh sách bạn bè của 1 user  theo id cuả user
router.get("/getAllDriver", driversController.getAllDriver);


module.exports = router;