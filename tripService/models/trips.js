const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const tripSchema = new Schema({

    startLocation: {
        type: String,
        required: true
    },
    endLocation: {
        type: String,
        required: true
    },
    distance: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    driverId: {
        type: Schema.Types.ObjectId,
        ref: "Driver"
    }
});

const Trip = mongoose.model("Trip", tripSchema);
module.exports = Trip;
