const Driver = require("../models/drivers");
const Car = require("../models/cars");
const Trip = require("../models/trips");
const axios = require('axios');


const getAllTrip = async (req, res) => {
    try {
        const token = req.cookies['accessToken'];
        const response = await axios.get(`${process.env.DRIVER_URL}/api/v1/driver/getAllDriver`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        const drivers = response.data;
        const trips = await Trip.find().populate('driverId', '_id name username gender');
        const tripsWithDriverData = trips.map(trip => {
            const driverData = drivers.find(driver => driver._id === trip.driverId._id);
            return {
                _id: trip._id,
                startLocation: trip.startLocation,
                endLocation: trip.endLocation,
                distance: trip.distance,
                price: trip.price,
                status: trip.status,
                driverId: {
                    _id: trip.driverId._id,
                    name: trip.driverId.name,
                    username: trip.driverId.username,
                    gender: trip.driverId.gender,
                }
            };
        });
        res.json(tripsWithDriverData);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Server error' });
    }
}
const deleteTrip = async (req, res) => {
    const { id } = req.params;
    try {
        const trip = await Trip.findByIdAndDelete(id);
        if (!trip) {
            return res.status(404).json({ message: 'No trip found with this id' });
        }
        res.json({ message: 'Trip deleted successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Server error' });
    }
}

const getTripById = async (req, res) => {
    const { id } = req.params;
    try {
        const trip = await Trip.findById(id);
        if (!trip) {
            return res.status(404).json({ message: 'No trip found with this id' });
        }
        const token = req.cookies['accessToken'];
        const response = await axios.get(`${process.env.DRIVER_URL}/api/v1/driver/getDriverById/${trip.driverId}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        const driverData = response.data;
        const tripWithDriverData = {
            _id: trip._id,
            startLocation: trip.startLocation,
            endLocation: trip.endLocation,
            distance: trip.distance,
            price: trip.price,
            status: trip.status,
            driverId: {
                _id: driverData._id,
                name: driverData.name,
                username: driverData.username,
                gender: driverData.gender,
                dateofbirth: driverData.dateofbirth,
                cars: driverData.cars

            }
        };

        res.json(tripWithDriverData);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

module.exports = {
    getAllTrip,
    getTripById,
    deleteTrip
};
