const { Router } = require("express");
const tripsController = require("../controller/tripsController");

const router = Router();

// http://localhost:3000/api/v1/users/login
router.get("/getAllTrip", tripsController.getAllTrip);
// http://localhost:3000/api/v1/users/sinup
router.get("/getTripById/:id", tripsController.getTripById);

router.delete("/deleteTrip/:id", tripsController.deleteTrip);


module.exports = router;