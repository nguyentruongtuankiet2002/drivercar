pipeline {
    agent any
    
    environment {
        // MONGODB_IMAGE_NAME = 'mongodb/mongodb-enterprise-server:latest'
        // REDIS_IMAGE_NAME = 'redis:latest'
        CARS_SERVICE_IMAGE_NAME = 'phunguyenst/backend_driver'
        DRIVER_SERVICE_IMAGE_NAME = 'phunguyenst/backend_driver'
        TRIP_SERVICE_IMAGE_NAME = 'phunguyenst/backend_driver'
        TRIPDRIVER_SERVICE_IMAGE_NAME = 'phunguyenst/backend_driver'
        HISTORY_SERVICE_IMAGE_NAME = 'phunguyenst/backend_driver'
        GATEWAY_IMAGE_NAME = 'phunguyenst/backend_driver'
        IMAGE_TAG = 'v1.0'
        // ZALO_APP_MOBILE_IMAGE_NAME = 'phunguyenst/zalo-mobile'
    }
    
    stages {
        stage('Prepare Environment') {
            steps {
                withCredentials([file(credentialsId: 'envGateway', variable: 'ENV_FILE_GATEWAY')]) {
                    sh 'cp $ENV_FILE_GATEWAY gatewayService/.env'
                }
                withCredentials([file(credentialsId: 'envCar', variable: 'ENV_FILE_CAR')]) {
                    sh 'cp $ENV_FILE_CAR carsService/.env'
                }
                withCredentials([file(credentialsId: 'envDriver', variable: 'ENV_FILE_DRIVER')]) {
                    sh 'cp $ENV_FILE_DRIVER driverService/.env'
                }
                withCredentials([file(credentialsId: 'envHistory', variable: 'ENV_FILE_HISTORY')]) {
                    sh 'cp $ENV_FILE_HISTORY historydriversService/.env'
                }
                withCredentials([file(credentialsId: 'envTripDriver', variable: 'ENV_FILE_TRIPDRIVER')]) {
                    sh 'cp $ENV_FILE_TRIPDRIVER tripdriversService/.env'
                }
                withCredentials([file(credentialsId: 'envTrip', variable: 'ENV_FILE_TRIP')]) {
                    sh 'cp $ENV_FILE_TRIP tripService/.env'
                }
            }
        }
        stage('Build') {
            parallel {
                stage('Build Cars Service') {
                    steps {
                        withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                            sh 'cd carsService && docker build -t $CARS_SERVICE_IMAGE_NAME:$IMAGE_TAG .'
                            sh 'docker push $CARS_SERVICE_IMAGE_NAME:$IMAGE_TAG'
                        }
                    }
                }
                stage('Build Driver Service') {
                    steps {
                        withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                            sh 'cd driverService && docker build -t $DRIVER_SERVICE_IMAGE_NAME:$IMAGE_TAG .'
                            sh 'docker push $DRIVER_SERVICE_IMAGE_NAME:$IMAGE_TAG'
                        }
                    }
                }
                stage('Build Trip Service') {
                    steps {
                        withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                            sh 'cd tripService && docker build -t $TRIP_SERVICE_IMAGE_NAME:$IMAGE_TAG .'
                            sh 'docker push $TRIP_SERVICE_IMAGE_NAME:$IMAGE_TAG'
                        }
                    }
                }
                stage('Build TripDriver Service') {
                    steps {
                        withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                            sh 'cd tripdriversService && docker build -t $TRIPDRIVER_SERVICE_IMAGE_NAME:$IMAGE_TAG .'
                            sh 'docker push $TRIPDRIVER_SERVICE_IMAGE_NAME:$IMAGE_TAG'
                        }
                    }
                }
                stage('Build History Service') {
                    steps {
                        withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                            sh 'cd historydriversService && docker build -t $HISTORY_SERVICE_IMAGE_NAME:$IMAGE_TAG .'
                            sh 'docker push $HISTORY_SERVICE_IMAGE_NAME:$IMAGE_TAG'
                        }
                    }
                }
                stage('Build Gateway') {
                    steps {
                        withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                            sh 'cd gatewayService && docker build -t $GATEWAY_IMAGE_NAME:$IMAGE_TAG .'
                            sh 'docker push $GATEWAY_IMAGE_NAME:$IMAGE_TAG'
                        }
                    }
                }
            }
        }
        stage('Deploy') {
            steps {
                sh 'docker rm -f $(docker ps -a | grep -v "my-jenkins" | cut -d " " -f1) || true'
                sh 'docker compose up -d'
                sh 'docker rmi $(docker images -f "dangling=true" -q) || true'
            }
        }
    }
    
    post {
        always {
            cleanWs()
        }
    }
}
