const express = require('express');
require('dotenv').config();
const cors = require('cors');
const cookieParser = require('cookie-parser');
const httpProxy = require('http-proxy');
const bodyParser = require('body-parser');

const middleware = require('./middleware/middlewareController.js');
const getaWayController = require('./controller/getaWayController.js');
const app = express();
const proxy = httpProxy.createProxyServer();

const PORT = 3000;

console.log("log driver url", process.env.DRIVER_URL);

// Middleware setup
app.use(cookieParser());
app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Proxy error handling
const handleProxyError = (err, req, res, target) => {
  console.error(`Error forwarding request to ${target}: ${err.message}`);
  res.status(500).send('Internal Server Error');
};

// Route definitions
app.use('/service0/login', getaWayController.login);
app.use('/service0/signup', getaWayController.signup);
app.use('/service1', middleware.verifyToken, (req, res) => {
  proxy.web(req, res, { target: process.env.CARS_URL }, (err) => handleProxyError(err, req, res, process.env.CARS_URL));
});

app.use('/service2',middleware.verifyToken, (req, res) => {
  proxy.web(req, res, { target: process.env.DRIVER_URL }, (err) => handleProxyError(err, req, res, process.env.DRIVER_URL));
});

app.use('/service3', middleware.verifyToken, (req, res) => {
  proxy.web(req, res, { target: process.env.HISTORY_URL }, (err) => handleProxyError(err, req, res, process.env.HISTORY_URL));
});

app.use('/service4', middleware.verifyToken, (req, res) => {
  proxy.web(req, res, { target: process.env.TRIPDRIVER_URL }, (err) => handleProxyError(err, req, res, process.env.TRIPDRIVER_URL));
});

app.use('/service5', middleware.verifyToken, (req, res) => {
  proxy.web(req, res, { target: process.env.TRIP_URL }, (err) => handleProxyError(err, req, res, process.env.TRIP_URL));
});
// Start server
app.listen(PORT, () => {
  console.log(`Server is running on PORT: ${PORT}`);
});
