const axios = require('axios');
const jwt = require("jsonwebtoken");
require("dotenv").config();

const login = async (req, res) => {
    const { username, password } = req.body;
    try {
        const requestData = {
            username: username,
            password: password
        };
        const response = await axios.post(`${process.env.DRIVER_URL}/api/v1/driver/login`, requestData);
        if (response.status === 200) {
            if (response.data) {
                const user = response.data;
                const accessToken = jwt.sign({
                    id: user._id,
                    name: user.name,
                    username: user.username,
                },
                    process.env.JWT_ACCESS_KEY,
                    { expiresIn: "1000s" },
                );
                const maxAge = 86400000; // Thời gian sống là 24 giờ (24 * 60 * 60 * 1000)
                res.cookie('accessToken', accessToken, { maxAge: maxAge });
                const { password, ...info } = user; // Loại bỏ trường password khi trả về thông tin người dùng
                return res.status(200).json({ ...info, accessToken });
            } else {
                return res.status(500).json({ error: "User data is missing or invalid" });
            }
        } else {
            // Nếu không thành công, trả về lỗi từ server khác
            return res.status(response.status).json(response.data);
        }
    } catch (error) {
        console.error('Error:', error.message);
        return res.status(500).json({ error: "Server error" });
    }
};

const signup = async (req, res) => {
    const { name, username, password, gender, cars } = req.body;
    try {
        // Kiểm tra xem username đã tồn tại trong cơ sở dữ liệu chưa
        const requestData = {
            name: name,
            username: username,
            password: password,
            gender: gender,
            cars: cars
        }
        const response = await axios.post(`${process.env.DRIVER_URL}/api/v1/driver/signup`, requestData);
        console.log(response.data);
        return res.json(response.data);
    } catch (error) {
        console.error('Error:', error.message);
        throw error;
    }
};

module.exports = {
    login,
    signup
};
