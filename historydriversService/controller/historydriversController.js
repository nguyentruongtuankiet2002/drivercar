const Driver = require("../models/drivers");
const Car = require("../models/cars");
const TripDriver = require("../models/tripdrivers");
const HistoryDriver = require("../models/historydrivers");
const axios = require('axios');
const CircuitBreaker = require('opossum');

const options = {
    timeout: 3000, // If our function takes longer than 3 seconds, trigger a failure
    errorThresholdPercentage: 50, // When 50% of requests fail, trip the circuit
    resetTimeout: 30000 // After 30 seconds, try again.
};

const axiosInstance = axios.create();

const breaker = new CircuitBreaker(axiosInstance.get, options);

breaker.fallback(() => Promise.reject(new Error('Service is currently down')));

breaker.on('fallback', (result) => console.log('Service is down. Fallback called.'));
breaker.on('reject', () => console.log('Request rejected.'));
breaker.on('timeout', () => console.log('Request timed out.'));
breaker.on('success', () => console.log('Request succeeded.'));
breaker.on('failure', () => console.log('Request failed.'));
breaker.on('open', () => console.log('Circuit breaker is open.'));
breaker.on('halfOpen', () => console.log('Circuit breaker is half open.'));
breaker.on('close', () => console.log('Circuit breaker is closed.'));

const getAllhistorydrivers = async (req, res) => {
    const newHistoryDriver = [];
    try {
        const historydrivers = await HistoryDriver.find();
        for (let i = 0; i < historydrivers.length; i++) {
            const driverId = historydrivers[i].driverId._id; // Lấy ra id của driver
            const tripDrivers = historydrivers[i].tripDrivers;
            const token = req.cookies['accessToken'];
            const driverResponse = await breaker.fire(`${process.env.DRIVER_URL}/api/v1/driver/getDriverById/${driverId}`);
            const driverData = driverResponse.data;
            const newHistoryDriverData = ({
                _id: historydrivers[i]._id,
                historyId: historydrivers[i].historyId,
                driverId: driverData,
                totalTrips: historydrivers[i].totalTrips,
                totalEarnings: historydrivers[i].totalEarnings,
                tripDrivers: historydrivers[i].tripDrivers
            });
            newHistoryDriver.push(newHistoryDriverData);
        }
        res.json(newHistoryDriver);
    } catch (error) {
        // Xử lý lỗi nếu có
        res.status(500).json({ message: error.message });
    }
}

module.exports = {
    getAllhistorydrivers,
};