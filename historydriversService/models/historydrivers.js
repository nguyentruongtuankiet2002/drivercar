const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const historyDriverSchema = new Schema({
    historyId: {
        type: Number,
        required: true
    },
    driverId: {
        type: Schema.Types.ObjectId,
        ref: "Driver"
    },
    totalTrips: {
        type: Number,
        required: true
    },
    totalEarnings: {
        type: Number,
        required: true
    },
    tripDrivers: [{
        type: Schema.Types.ObjectId,
        ref: "TripDriver"
    }]
});

const HistoryDriver = mongoose.model("HistoryDriver", historyDriverSchema);
module.exports = HistoryDriver;
