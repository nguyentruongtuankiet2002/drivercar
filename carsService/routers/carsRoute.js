const { Router } = require("express");
const carsController = require("../controller/carsController");

const router = Router();

// http://localhost:3000/api/v1/users/login
router.post("/addCar", carsController.addCar);
// http://localhost:3000/api/v1/users/sinup
router.get("/getAllCar", carsController.getAllCar);
// lấy ra danh sách bạn bè của 1 user  theo id cuả user
router.get("/getCarById/:id", carsController.getCarById);

router.get("/check-redis", carsController.checkRedis);


module.exports = router;