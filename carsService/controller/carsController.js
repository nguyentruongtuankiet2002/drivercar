
const Car = require("../models/cars");
const redisClient = require("../redis/redisController");
const bcrypt = require("bcrypt");
const client = require("../redis/redisController");

const addCar = async (req, res) => {
    const { name } = req.body;
    try {
        const car = new Car({
            name: name,
        });
        await car.save();
        res.status(200).json(car);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

 // để test vào terminal gõ redis-cli sau đó gõ GET cars
 // để xem dữ liệu đã được lưu vào redis chưa
 // để xóa gõ DEL cars
    // để xem thời gian hết hạn của key gõ TTL cars
const getAllCar = async (req, res) => {
    try {

        const cachedCars = await redisClient.get('cars');
        if (cachedCars) {
            console.log('Dữ liệu có sẵn trong Redis');
            return res.status(200).json(JSON.parse(cachedCars));
        }
        const cars = await Car.find();
        // Lưu danh sách xe vào Redis với thời gian hết hạn (TTL) là 1 giờ
        console.log('Không có dữ liệu trong Redis, lưu dữ liệu vào Redis');
        await redisClient.setEx('cars', 3600, JSON.stringify(cars)); // TTL là 3600 giây (1 giờ)
        res.status(200).json(cars);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

// để test vào terminal gõ redis-cli sau đó gõ GET car:60aae4843ae33121e0de8503

const getCarById = async (req, res) => {
    const { id } = req.params;
    try {
        const cachedCar = await redisClient.get(`car:${id}`);
        if (cachedCar) {
            console.log('Dữ liệu có sẵn trong Redis');
            return res.status(200).json(JSON.parse(cachedCar));
        }
        const car = await Car.findById(id);
        if (!car) {
            return res.status(404).json({ error: "Car not found" });
        }
        console.log('Không có dữ liệu trong Redis, lưu dữ liệu vào Redis');
        await redisClient.setEx(`car:${id}`, 3600, JSON.stringify(car)); // TTL là 3600 giây (1 giờ)
        res.status(200).json(car);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};
const checkRedis = async (req, res) => {
    try {
        const reply = await client.ping();
        if (reply === 'PONG') {
            res.send('Redis connection is working');
        } else {
            res.status(500).send('Error checking Redis connection');
        }
    } catch (err) {
        res.status(500).send('Error checking Redis connection');
    }
};

const test = async (req, res) => {
    res.send('Hello World!');
};


module.exports = {
    addCar,
    getAllCar,
    getCarById,
    checkRedis,
    test,
};
