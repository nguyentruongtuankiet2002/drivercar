const Driver = require("../models/drivers");
const Car = require("../models/cars");
const axios = require('axios');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require('dotenv').config({ path: '.env' });


const login = async (req, res) => {
    const { username, password } = req.body;
    try {
        const user = await Driver.findOne({
            username: username,
        }).populate("cars");
        if (!user) {
            return res.status(400).json({ error: "Username is incorrect" });
        }
        console.log("Tôi là kiệt");
        const validPassword = await bcrypt.compare(password, user.password);
        if (!validPassword) {
            return res.status(400).json({ error: "Password is incorrect" });
        }
        if (user && validPassword) {
            const accessToken = jwt.sign({
                id: user._id,
                name: user.name,
                username: user.username,
            },
                process.env.JWT_ACCESS_KEY,
                { expiresIn: "1000s" },
            );
            const maxAge = 86400000; // Thời gian sống là 24 giờ (24 * 60 * 60 * 1000)
            res.cookie('accessToken', accessToken, { maxAge: maxAge });
            const { password, ...info } = user._doc;
            res.status(200).json({ ...info, accessToken });
        }
    }
    catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};


const signup = async (req, res) => {
    const { name, username, password, gender, cars } = req.body;
    try {
        // Kiểm tra xem username đã tồn tại trong cơ sở dữ liệu chưa
        const user = await Driver.findOne({ username: username });
        if (user) {
            return res.status(400).json({ error: "Username is already taken" });
        }
        // Tạo salt và hash password
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);
        // Tạo tài khoản mới cho tài xế
        const newUser = new Driver({
            name: name,
            username: username,
            password: hashedPassword,
            gender: gender,
            cars: cars
        });
        // Lưu tài khoản mới vào cơ sở dữ liệu
        await newUser.save();
        // Trả về tài khoản mới đã được tạo
        res.status(200).json(newUser);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const getAllDriver = async (req, res) => {
    try {
        const drivers = await Driver.find().populate("cars");
        const arrDriver = [];
        for (const driver of drivers) {
            const cars1 = [];
            for (const carId of driver.cars) {
                const response = await axios.get(`${process.env.CARS_URL}/api/v1/cars/getCarById/${carId._id}`);
                cars1.push(response.data);
            }
            const newDriver = ({
                _id: driver._id,
                dateofbirth: driver.dateofbirth,
                gender: driver.gender,
                name: driver.name,
                username: driver.username,
                password: driver.password,
                cars: cars1,
            });
            arrDriver.push(newDriver);
        }
        res.status(200).json(arrDriver);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
}


const findDriverById = async (req, res) => {
    const { id } = req.params;
    try {
        const tripdriver = await Driver.findById(id);
        if (!tripdriver) {
            return res.status(404).json({ message: 'No driver found with this id' });
        }
        const cars1 = [];
        for (const carId of tripdriver.cars) {
            console.log("url car" +process.env.CARS_URL);
            const response = await axios.get(`${process.env.CARS_URL}/api/v1/cars/getCarById/${carId._id}`);
            cars1.push(response.data); // Thêm thông tin chi tiết của xe vào mảng cars
        }
        const newDriver = ({
            _id: tripdriver._id,
            dateofbirth: tripdriver.dateofbirth,
            gender: tripdriver.gender,
            name: tripdriver.name,
            username: tripdriver.username,
            password: tripdriver.password,
            cars: cars1,
        });
        res.json(newDriver);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}


module.exports = {
    login,
    signup,
    getAllDriver,
    findDriverById
};
