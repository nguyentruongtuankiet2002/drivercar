const { Router } = require("express");
const driversController = require("../controller/driversController");
const middlewareController = require("../middleware/middlewareController");

const router = Router();

// http://localhost:3000/api/v1/users/login
router.post("/login", driversController.login);
// http://localhost:3000/api/v1/users/sinup
router.post("/sinup", driversController.signup);
// lấy ra danh sách bạn bè của 1 user  theo id cuả user
router.get("/getAllDriver", driversController.getAllDriver);


router.get("/getDriverById/:id", driversController.findDriverById);

router.get("/error-endpoint", (req, res) => {
    res.status(500).send('This is a forced error');
});
module.exports = router;